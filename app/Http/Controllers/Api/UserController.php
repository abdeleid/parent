<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    /**
     * Listing of users
     * @urlParam provider string optional e.g. provider=DataProviderX.
     * @urlParam statusCode string  optional e.g. statusCode=authorised.
     * @urlParam  balanceMin=10&balanceMax=100 string optional users within the min and max balance.
     * @urlParam  currency string optional users with specific currency e.g.currency=EUR.
     * @param Request $request
     * @return mixed
     */
    public function index()
    {
        $users = [];

        $this->readAndMerge($users);

        $users = collect($users)
            ->when(request()->provider, function ($users) {
                return $users->where('provider', request()->provider);
            })
            ->when(request()->statusCode == 'authorised', function ($users) {
                return $users->filter(function ($user) {
                    if (array_key_exists('statusCode', $user)) {
                        return $user['statusCode'] == 1;
                    } else {
                        return $user['status'] == 100;
                    }
                });
            })
            ->when(request()->statusCode == 'decline', function ($users) {
                return $users->filter(function ($user) {
                    if (array_key_exists('statusCode', $user)) {
                        return $user['statusCode'] == 2;
                    } else {
                        return $user['status'] == 200;
                    }
                });
            })
            ->when(request()->statusCode == 'refunded', function ($users) {
                return $users->filter(function ($user) {
                    if (array_key_exists('statusCode', $user)) {
                        return $user['statusCode'] == 3;
                    } else {
                        return $user['status'] == 300;
                    }
                });
            })
            ->when(request()->balanceMin && request()->balanceMax, function ($users) {
                return $users->filter(function ($user) {
                    if (array_key_exists('balance', $user)) {
                        return $user['balance'] >= request()->balanceMin && $user['balance'] <= request()->balanceMax;
                    } else {
                        return $user['parentAmount'] >= request()->balanceMin && $user['parentAmount'] <= request()->balanceMax;
                    }
                });
            })
            ->when(request()->currency, function ($users) {
                return $users->filter(function ($user) {
                    if (array_key_exists('Currency', $user)) {
                        return $user['Currency'] == request()->currency;
                    } else {
                        return $user['currency'] == request()->currency;
                    }
                });
            });

        return $users;

    }// end of index

    protected function readAndMerge(&$users)
    {
        $path = public_path('users');
        $files = File::allFiles($path);

        foreach ($files as $index => $file) {

            //strpos is faster than substr and preg_match
            //can add any file that start with DataProvider e.g DataProviderZ
            if (strpos($file->getFilename(), 'DataProvider') === 0) {

                $content = json_decode(file_get_contents(public_path('users/' . $file->getFilename())), true)['users'];

                for ($i = 0; $i < count($content); $i++) {

                    $content[$i]['provider'] = $file->getFilenameWithoutExtension();
                    array_push($users, $content[$i]);

                }//end of for each

            }//end of if statement

            continue;

        }//end of for each

    }// end of mergeArrays

}//end of controller
